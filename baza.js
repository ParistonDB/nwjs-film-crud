var mongoose          = require('mongoose');

mongoose.connect('mongodb://localhost/movie', function(err) {
  if(err) { console.log(err); }
  else { console.log('Connected to the database'); }
});

var movie = mongoose.model('movies', new mongoose.Schema({
    _id:          String,
    title:        String,
    director:     String,
    description:  String,
    plus18:       Boolean
}));

var wyszukiwanie =   function() {
  $("body h1, tr.movies2").each(function() {
    $(this).remove();
  });

  movie.find({}, function(err, result) {
    if(err) { console.log('There was a problem with getting all the movies.'); }
    else {
      if(result.length === 0) {
        $(".movies, .movie_edit_container").hide();
        $("<h1>There is no movies in database.</h1>").insertAfter(".movies");
      } else {
        for(var i = 0; i < result.length; i++) {
          $(".movies, .movie_edit_container").show();
          var content = "";
          if(result[i].plus18 === false) {
            content += "<tr class='movies2'><td>"+(i+1)+"</td>";
          } else {
            content += "<tr class='movies2'><td>"+(i+1)+"<div class='adults'>+18</div></td>";
          }
          content +=    "<td>"+result[i].title+"</td>"+
                        "<td>"+result[i].director+"</td>"+
                        "<td>"+result[i].description+"</td>"+
                        "<td><input type='submit' id='movie_delete' value='Delete' onclick='movie_delete("+JSON.stringify(result[i]._id)+")'>&nbsp;&nbsp;"+
                        "<input type='submit' id='movie_edit_select' value='Select' onclick='select_movie("+JSON.stringify(result[i]._id)+")'></td></tr>";
          $(".movies").append(content);
        }
      }
    }
  });
};

$().ready(function() {
  //var content = "";

  movie.find({}, function(err, result) {
    if(err) { console.log('There was a problem with getting all the movies.'); }
    else {
      if(result.length === 0) {
        $(".movies, .movie_edit_container").hide();
        $("<h1>There is no movies in database.</h1>").insertAfter(".movies");
      } else {
        for(var i = 0; i < result.length; i++) {
          $(".movies, .movie_edit_container").show();
          var content = "";
          if(result[i].plus18 === false) {
            content += "<tr class='movies2'><td>"+(i+1)+"</td>";
          } else {
            content += "<tr class='movies2'><td>"+(i+1)+"<div class='adults'>+18</div></td>";
          }
          content +=    "<td>"+result[i].title+"</td>"+
                        "<td>"+result[i].director+"</td>"+
                        "<td>"+result[i].description+"</td>"+
                        "<td><input type='submit' id='movie_delete' value='Delete' onclick='movie_delete("+JSON.stringify(result[i]._id)+")'>&nbsp;&nbsp;"+
                        "<input type='submit' id='movie_edit_select' value='Select' onclick='select_movie("+JSON.stringify(result[i]._id)+")'></td></tr>";
          $(".movies").append(content);
        }
      }
    }
  });
});

var select_movie = function(id) {

  movie.findOne({ _id: id }, function(err, result) {
    if(err) { console.log('There was a problem with selecting this movie.'); next(); }
    else {
      $("#edit_movie_title").val(result.title);
      $("#edit_movie_director").val(result.director);
      $("#edit_movie_description").val(result.description);
      if (result.plus18 === true) {
        $("#edit_movie_plus18").attr('checked', true);
      }
      $("#edit_movie_id").val(result._id);
    }
  });
};

var movie_add = function() {
  var plus18 = false;
  if ($('#plus18').is(':checked')) {
    plus18 = true;
  }

  var empty = 0;  //an amount of empty inputs in form
  $("#movie_add_container input[type='text']").each(function() {
      if($(this).val() === "") empty++;
  });

  if(empty === 0) {
    new movie({
      _id:          mongoose.Types.ObjectId(),
      title:        $("#movie_title").val(),
      director:     $("#movie_director").val(),
      description:  $("#movie_description").val(),
      plus18:       plus18
    }).save(function(err) {
      if(err) { console.log(err); }
      else {
        console.log("A movie has been added.");
      }
    });
  } else {
    alert("Fill all the inputs fields");
  }

  $("#movie_title").val("");
  $("#movie_director").val("");
  $("#movie_description").val("");

  wyszukiwanie();
};

var movie_edit = function(id) {
  var plus18 = false;

  if ($('#edit_movie_plus18').is(':checked')) {
    plus18 = true;
  }

  movie.find({ _id: $("#edit_movie_id").val() }, function(err, result) {
    if(err) { console.log('There was a problem with getting this movie.'); }
    else { console.log(result); }
  }).update({
    title: $("#edit_movie_title").val(),
    director: $("#edit_movie_director").val(),
    description: $("#edit_movie_description").val(),
    plus18: plus18
  }).exec();

  wyszukiwanie();

  $("#edit_movie_title").val("");
  $("#edit_movie_director").val("");
  $("#edit_movie_description").val("");
  $("#edit_movie_plus18").attr('checked', false);
  $("#edit_movie_id").val("");
};

var movie_delete = function(id) {
  movie.find({ _id: id }, function(err, result) {
    if(err) { console.log('There was a problem with getting this movie.'); }
  }).remove(function(err) {
    if(err) { console.log("There was a problem with removing this movie"); }
    else { console.log("This movie has been removed."); }
  });

  wyszukiwanie();
};
